#! /usr/bin/env python3

# Copyright (c) 2016 Phillip Alday
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bibtexparser
import re
from copy import deepcopy

from argparse import ArgumentParser, FileType, ArgumentDefaultsHelpFormatter
from multiprocessing import Pool, TimeoutError
import functools as ft

import sys

from bibtexparser.bparser import logger
from logging import NullHandler
logger.addHandler(NullHandler())

def main(argv=None):
    parser = ArgumentParser(description='Find duplicates in a BibTeX file',
                            epilog='Although the order of the fields does not make any '
                                    'in terms of which duplicates are extracted, the first'
                                    ' field determines sorting and grouping when using '
                                    'intersection to join multiple fields.')
                            #formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('bibtex_file', type=FileType('r'),
                        help='Input file (BibTeX format)')
                        
    parser.add_argument('--invert', '-i', action='store_true', default=False,
                        help='Invert match (i.e. find non-duplicates / unique entries)')

    standardize = parser.add_mutually_exclusive_group()
    standardize.add_argument('--standardize', '-s', action='store_true', default=True,
                             help='Standardize strings so that punctuation, capitalization and'
                             " whitespace differences don't impact matching (default)")
    standardize.add_argument('--exact', '-e', action='store_false',dest='standardize',
                             help='Do not standardize, force exact matches.')
    
    print_stdout = parser.add_mutually_exclusive_group()
    print_stdout.add_argument('--print-grouped-keys', action='store_true', default=False,
                        help='Print groups of duplicate entries (default)')

    print_stdout.add_argument('--print-keys', action='store_true', default=False,
                        help='Print simple list of all keys that occur as duplicates, '
                             'sorted lexicographically (disables implicit --print-keys)')
                        
    parser.add_argument('--write-bibtex',metavar='OUTPUT_FILE', type=FileType('w',encoding='UTF-8'), default=None,
                        help='Write BibTeX with results (disables implicit --print-keys)')
                        
    parser.add_argument('--field','-f',default='title',nargs='+',
                        help='Field(s) to perform matching on. Default is "title"')
                        
    join = parser.add_mutually_exclusive_group()
    join.add_argument('--intersection',action='store_true',default=True,
                      help='Use intersection of duplicate matches across field (default)')
    join.add_argument('--union',action='store_false',dest='intersection',
                      help='Use union of duplicate matches across field')
                      
    parser.add_argument('--jobs', '-j', default=None, type=int,
                        help='Number of multiprocessing jobs to use'
                             ' (for processing multiple fields in parallel)')
 
    opts = parser.parse_args()
    
    # default to dumping list of grouped keys
    if not opts.print_keys and not opts.write_bibtex:
        opts.print_grouped_keys = True
       
    bibtex_database = bibtexparser.load(opts.bibtex_file)

    # preparing for pickling in Pool.map()
    find_dups = ft.partial(find_duplicates_by_field, 
                           bibtex_database.entries_dict, invert=opts.invert, 
                           standardize=opts.standardize)

    with Pool(processes=opts.jobs) as pool:
        duplicates = pool.map(find_dups,
                          opts.field)
              
    duplicates = list(duplicates)
    # list of sets of flagged entries
    dups = [ set([bibentry for tup in di for bibentry in di[tup]] ) for di in duplicates]
    
    if not opts.intersection:
        if len(opts.field) > 1:
            common = dups[0].union(*dups[1:])
        else:
            common = dups[0]

        if opts.print_grouped_keys:
            for dups_by_field, field in zip(duplicates, opts.field):
                print(field)

                for d in sorted(dups_by_field):
                    print("  {}".format(d))
                    print("      {}".format(", ".join(dups[d])))
    else:           
        if len(opts.field) > 1:
            common = dups[0].intersection(*dups[1:])
        else:
            common = dups[0]
        
        if opts.print_grouped_keys:
            for d in sorted(duplicates[0]):
                hits = [ x for x in duplicates[0][d] if x in common]
                if hits:
                    print("  {}".format(d))
                    print("      {}".format(", ".join(hits)))    
    

    if opts.print_keys:    
        for d in common: 
            print(d)
            
    if opts.write_bibtex:
        entries = bibtex_database.entries_dict
        entries = [entries[be] for be in entries if be in common]
        print(entries[0])
        bibtex_out = bibtexparser.bibdatabase.BibDatabase()
        bibtex_out.entries = entries
        bibtexparser.dump(bibtex_out, opts.write_bibtex)
        


def find_duplicates_by_field(entry_dict, field='title', invert=False, standardize=True):
    entry_dict = deepcopy(entry_dict)
    
    # we can only check for duplicates on a field if that field is actually present
    entry_dict = {e:entry_dict[e] for e in entry_dict if field in entry_dict[e]}
    
    if standardize:
        f = lambda e: (re.sub('\W','#',entry_dict[e][field].casefold().strip()), e)
    else:
        f = lambda e: (entry_dict[e][field], e)

    ids = map(f, entry_dict)
  
    dups = dict()
    for i in ids:
        key = i[0]
        value = i[1]
        
        if key not in dups:
            dups[key] = [value]
        else:
            dups[key].append(value)
            
    return {k:dups[k] for k in dups if (len(dups[k]) > 1) != invert}
    
if __name__ == '__main__':
    sys.exit(main())


    
    